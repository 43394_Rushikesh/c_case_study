#include<stdio.h>
#include<string.h>
#include "library.h"

void user_accept(user_t *u){
    printf("id : ");
    scanf("%d", &u->id);
    printf("Name : ");
    scanf("%s", u->name);
    printf("email : ");
    scanf("%s",u->email);
    printf("phoone : ");
    scanf("%s",u->phone);
    printf("password : ");
    scanf("%s",u->password);
    strcpy(u->role,ROLE_MEMBER);


}
void user_dislpay(user_t *u){
    printf("%d , %s , %s , %s , %s \n",u->id,u->name,u->email,u->phone,u->role );

}
void book_accept(book_t *b) {
	printf("id: ");
	scanf("%d", &b->id);
	printf("name: ");
	scanf("%s", b->name);
	printf("author: ");
	scanf("%s", b->author);
	printf("subject: ");
	scanf("%s", b->subject);
	printf("price: ");
	scanf("%lf", &b->price);
	printf("isbn: ");
	scanf("%s", b->isbn);
}

void book_display(book_t *b) {
	printf("%d, %s, %s, %s, %.2lf, %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}
void bookcopy_accept(bookcopy_t *c){
	printf("id : ");
	scanf("%d",&c->id);
	printf("name : ");
	scanf("%d", &c->bookid);
	printf("author : ");
	scanf("%d", &c->rack);
	strcpy(c->status,STATUS_AVAIL);

}
void bookcopy_display(bookcopy_t *c){
	printf("%d, %d ,%d ,%s ", c->id,c->bookid,c->rack,c->status);

}

void issuerecord_accept(issuerecord_t *r){
	printf("id : ");
	scanf("%d",&r->id);
	printf("copy id: ");
	scanf("%d",&r->copyid);
	printf("member id: ");
	scanf("&d",&r->memberid);
	printf("issue date");
	date_accept(&r->issue_date);
	r->return_duedate = date_add(r->issue_date,BOOK_RETURN_DAYS);
	memset(&r->return_date,0,sizeof(date_t));
	r->fine_amount = 0.0;
}
void issuerecord_display(issuerecord_t *r){
	printf("issue record: %d , copy: %d, member: %d, find: %.2lf\n",r->id,r->copyid,r->memberid,r->fine_amount);
	printf("issue date");
	date_print(&r->issue_date);
	printf("return date");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);

}

void payment_accept(payment_t *p){
	printf("id: ");
	scanf("%d ",&p->id);
	printf("member id: ");
	scanf("%d", &p->memberid);
	printf("type(fees/fine): ");
	scanf("%s", p->type);
	printf("amoint: ");
	scanf("%d", &p->amount);
	p->tx_time = date_current();
	if(strcmp(p->type,PAY_TYPE_FEES) == 0)
	p->next_pay_duedate = date_add(p->tx_time,MEMBERSHIP_MONTH_DAYS);
	else
	memset(&p->next_pay_duedate,0,sizeof(date_t));

}
void payment_display(payment_t *p){
	printf("payment: %d , member: %d, %s,amount: %.2lf\n",p->id,p->memberid,p->type,p->amount);
	printf("paymrnt");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}
void user_add(user_t *u) {
	
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}

	
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");
	
	
	fclose(fp);
}
void book_find_by_name(char name[]){
	FILE *fp;
	int found = 0;
	book_t b;
	//open file 
	fp = fopen(BOOK_DB , "rb");
	if(fp == NULL){
		perror("failed to open book file");
		return;
	}
	//read all book for finding book
	while(fread(&b,sizeof(book_t),1,fp)>0){
		//if name is matching partially
		if(strstr(b.name,name) != NULL){
			found = 1;
			book_display(&b);
		}
	}
	//close file
	fclose(fp);
	if(!found)
	printf("No such book found\n");
	
}

int user_find_by_email(user_t *u, char email[]) {
	FILE *fp;
	int found = 0;
	
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}
	
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
		
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	
	fclose(fp);
	
	return found;
}
int get_next_user_id(){
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;
	//open file
	fp= fopen(USER_DB,"rb");
	if(fp == NULL)
         return max+1;
	//change file pos
	fseek(fp, -size , SEEK_END);
	//read record from file
	if(fread(&u , size ,1,fp)>0)
	//if read is success , get max id
	max = u.id;
	//close file
	fclose(fp);
	return max+1;
}

int get_next_book_id(){
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	//open file
	fp= fopen(BOOK_DB,"rb");
	if(fp == NULL)
         return max+1;
	//change file pos
	fseek(fp, -size , SEEK_END);
	//read record from file
	if(fread(&u,size , 1,fp)>0)
	//if read is success , get maz (its)id
	max = u.id;
	//close file
	fclose(fp);
	return max+1;
	
}
int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;
	// open the file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

