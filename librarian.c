#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void librarian_area(user_t *u){
    int choice;
    char name[90];
 do{
    printf("\n\n0. Sign Out\n1. Add Member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8 add new copy \n9. Change rack \n10. Issue Copy \n11. Return Copy\n12. Take Payment \n13. Payment History\nEnter choice:");
    scanf("%d",&choice);
    switch (choice)
    {
    case 1: add_member();
        break;
    case 2: 
        break;
    case 3 :
        break;
    case 4: add_book();
        break;
    case 5 : printf("Enter book name");
             scanf("%s",name);
             book_find_by_name(name);

        break;
    case 6 : book_edit_by_id();
        break;
    case 7:  bookcopy_checkavailable_details();
        break;
    case 8: bookcopy_add();
        break;
    case 9 :
        break;
    case 10: bookcopy_issue();
        break;
    case 11: bookcopy_return();
        break;
    case 12:  
        break;
    case 13:
        break;
      }
   }while ((choice!=0));

}
void add_member(){
    user_t u;
    user_accept(&u);

    user_add(&u);
}
void add_book(){
    FILE *fp;
    book_t b;
    book_accept(&b);
    b.id = get_next_book_id();
    fp = fopen(BOOK_DB, "ab");
    if(fp== NULL){
        perror("cannot opn book file");
        exit(1);
    }
    fwrite(&b,sizeof(book_t), 1 ,fp);
    printf("book added into file");
    //close book file
    fclose(fp);
}
void book_edit_by_id(){
    int id ,found = 0;
    FILE *fp;
    book_t b;
    // input book id from user
    printf("Enter book id");
    scanf("%d",&id);
    //open book files
    fp = fopen(BOOK_DB, "rb+");
    if (fp == NULL){
        perror("cannot open book file");
        exit(1);
    }
    //read book one by one for id
    while(fread(&b, sizeof(book_t),1, fp)>0){
        if(id == b.id){
            found = 1;
            break;
        }
    }
//if found
if(found){
    //input new details
    long size = sizeof(book_t);
    book_t nb;
    book_accept(&nb);
    nb.id = b.id;
    //take fil pos one rec behind
    fseek(fp, -size , SEEK_CUR);
    //overwrite book details
    fwrite(&nb, size , 1 , fp);
    printf("Book updated \n");
}
else // if not found
printf("Book not found\n");
fclose(fp);
}
void bookcopy_checkavailable_details(){
    int book_id;
    FILE *fp;
    bookcopy_t bc;
    int count = 0;
    //Ip book id
    printf("Enter book id");
    scanf("%d",&book_id);
    //open book copies file
    fp = fopen(BOOKCOPY_DB,"rb");
    if (fp == NULL){
        perror("cannot open file");
        return;
    }
    //read book copies record 
    while(fread(&bc, sizeof(bookcopy_t),1,fp)>0){
        //if book id match and available print status
        if(bc.bookid == book_id && strcmp(bc.status,STATUS_AVAIL)==0){
            bookcopy_display(&bc);
            count++;
        }
        }
        //close book copies file
        fclose(fp);
        //if no copy available
        if(count==0){
            perror("no copies available\n");
        }
    
}
void bookcopy_add(){
    FILE *fp;
    //input book copy details
    bookcopy_t b;
    bookcopy_accept(&b);
    b.id = get_next_bookcopy_id();
    // add book copy into file
    //open file for copies
    fp = fopen(BOOKCOPY_DB,"ab");
    if(fp == NULL){
        perror("cannot open book copies file");
        exit(1);

    }
    //append book copy to file
    fwrite(&b,sizeof(bookcopy_t),1,fp);
    printf("book copy added into file");
    //close book copies file
    fclose(fp);
}
void bookcopy_issue(){
    issuerecord_t rec;
    FILE *fp;
    //accept issurecord details from user
    issuerecord_accept(&rec);
    //idf user not paid get error
    //generate and assign new id for issue record 
    rec.id = get_next_issuerecord_id();
    //open issue record fie
     fp = fopen(ISSUERECORD_DB, "ab");
     if(fp == NULL){
         perror("issue record file cannot be open");
         exit(1);
     }
     //append record into file
     fwrite(&rec , sizeof(issuerecord_t),1 , fp);
     //close file
     fclose(fp);
     // mark copy as issued
     bookcopy_changestatus(rec.copyid,STATUS_ISSUED);
}
void bookcopy_changestatus(int bookcopy_id, char status[]) {
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}

	// read book copies one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if bookcopy id is matching
		if(bookcopy_id == bc.id) {
			// modify its status
			strcpy(bc.status, status);
			// go one record back
			fseek(fp, -size, SEEK_CUR);
			// overwrite the record into the file
			fwrite(&bc, sizeof(bookcopy_t), 1, fp);
			break;
		}
	}
	
	// close the file
	fclose(fp);
}
void bookcopy_return(){
    int member_id , record_id;
    FILE *fp;
    issuerecord_t rec;
    int diff_days,found = 0;
    long size = sizeof(issuerecord_t);
    //input member id
    printf("enter member id");
    scanf("%d",&member_id);
    //print issued book 
    display_issued_bookcopies(member_id);
    //input issue record to be return
    printf("enter issue record id(to return)");
    scanf("%d",&record_id);
    //open issue record file
    fp = fopen(ISSUERECORD_DB,"rb+");
   if (fp==NULL){
       perror("cannot open issue record file");
       return;
   }
  // read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// find issuerecord id
	if(record_id == rec.id) {
		found = 1;
			// initialize return date
		rec.return_date = date_current();
			// check for the fine amount
		diff_days = date_cmp(rec.return_date, rec.return_duedate);
			// update fine amount if any
		if(diff_days > 0)
		rec.fine_amount = diff_days * FINE_PER_DAY;
	 	break;
		}
	}
	
    	if(found) {
		// go one record back
     	fseek(fp, -size, SEEK_CUR);
  		// overwrite the issue record
	   fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		// print updated issue record.
     	printf("issue record updated after returning book:\n");
    	issuerecord_display(&rec);
		// update copy status to available 
    	bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
}
	
	// close the file.
	fclose(fp);
}
void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	//open issue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
	perror("cannot open issue record file");
	return;
    }
	//read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
	// if member_id is matching and return date is 0, print it.
	if(rec.memberid == member_id && rec.return_date.day == 0)
	issuerecord_display(&rec);
	}
	//close the file
	fclose(fp);
}



